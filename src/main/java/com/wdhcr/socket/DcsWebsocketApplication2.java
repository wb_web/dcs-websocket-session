package com.wdhcr.socket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DcsWebsocketApplication2 {

    public static void main(String[] args) {
        SpringApplication.run(DcsWebsocketApplication2.class, args);
    }

}
